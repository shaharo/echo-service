redis.log(redis.LOG_WARNING,'KEY:', KEYS[1])
redis.log(redis.LOG_WARNING,'SCORE_START:' , ARGV[1])
redis.log(redis.LOG_WARNING,'LIMIT:' , ARGV[2])
local table = redis.call('zrangebyscore',KEYS[1],0, ARGV[1] ,'WITHSCORES', 'LIMIT' ,0, ARGV[2])
local resultTable = {}
for i=1,#table do
  if i % 2 ~= 0 then
      local result = redis.call('ZREM',KEYS[1], table[i])
      redis.log(redis.LOG_WARNING, 'RESULT' , result)
      redis.log(redis.LOG_WARNING, table[i])
      resultTable[#resultTable+1] = table[i]
  end
end
return resultTable