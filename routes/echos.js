const express = require('express');
const router = express.Router();
const uuidv4 = require('uuid/v4');
const asyncMiddleware = require('../middleware/async');
const {validate,scheduleEcho} = require('../models/echo');


router.post("/", asyncMiddleware( async (req,res,next) => {
    const {error} = validate(req.body); 
    if(error) return res.status(400).send(error.details[0].message);

    let echoTimestamp = req.body.timestamp;
    let echoRequest = { id:uuidv4(), message: req.body.message, timestamp: echoTimestamp}

    const resultZadd = await scheduleEcho(echoTimestamp,echoRequest);
    res.send(echoRequest).status(200);
}));

module.exports = router;