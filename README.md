[![pipeline status](https://gitlab.com/shaharo/moonactive-test/badges/master/pipeline.svg)](https://gitlab.com/shaharo/moonactive-test/commits/master)


# echo-service

Service that enables creating echo requests at a specific timestamp in the future with a message , and then display it on the server console once the time has passed.
The backend used for storing the messages is redis and it's sorted set data structure.

API Example:

POST /api/echos HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Cache-Control: no-cache

{
	"message": "hello!!!",
	"timestamp": 1550646095000
}

Validation:

- message should be a non empty string up to 255 characters
- timestamp should be a future milliseconds timestamp


Running Docker:

- docker run -e NODE_ENV=production -p 8080:8080 {IMAGE}
