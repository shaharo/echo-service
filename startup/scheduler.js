const {fetchEchos} = require('../models/echo');
const winston = require('winston');


function parseEchos(echosResult) {
    if(echosResult.length>0) {
        let echos = echosResult.map( x => JSON.parse(x));
        return echos;
    }
    return [];
}


async function displayEchoAsync() {
    let echoTimestamp = Date.now();
    const redisResult = await fetchEchos(echoTimestamp);
    const echos = parseEchos(redisResult);
    if(echos.length > 0)
        printEchos(echos);
}

function printEchos(echo) {
    echo.forEach(element => {
        winston.info(element.message + " - " + element.timestamp);
    });
}


exports.parseEchos = parseEchos;
module.exports = function() {
    setInterval(displayEchoAsync,1000);
}


