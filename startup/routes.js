const echos = require('../routes/echos');
const health = require('../routes/health');
const express = require('express');
const error = require('../middleware/error');

module.exports = function(app) {
    app.use(express.json());
    app.use('/health',health)
    app.use('/api/echos',echos);
    app.use(error);
}