const config = require('config');
const Redis = require('ioredis');
const fs = require('fs');

var redisClient = new Redis({
    host: config.get('redis.host'),
    port: config.get('redis.port'),
    lazyConnect: true,
});
//added lua
let script = fs.readFileSync('./lua_scripts/get_echos.lua',{encoding: 'utf8'});
redisClient.defineCommand('get_echos', {
  numberOfKeys: 1,
  lua: script
});


module.exports.redis = redisClient;