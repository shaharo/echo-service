const Joi = require('joi');
const config = require('config');
const db = require('../startup/db');

let sortedSetKey = config.get('redis.sorted_set_key');
let echosFetchLimit = config.get('redis.echos_fetch_limit');

async function scheduleEcho(echoTimestamp,echo) {
    await db.redis.zadd(sortedSetKey,"nx",echoTimestamp,JSON.stringify(echo));
}

async function fetchEchos(echoTimestamp) {
    let result = await db.redis.get_echos(sortedSetKey,echoTimestamp,echosFetchLimit);
    return result;
}

function validateEcho(echo) {
    let currentTimestamp = Date.now();
    const schema = { 
        message: Joi.string().min(1).max(255).required(),
        timestamp: Joi.date().timestamp('javascript').greater(currentTimestamp) 
    };
    return Joi.validate(echo,schema);
}

exports.validate = validateEcho;
exports.scheduleEcho = scheduleEcho;
exports.fetchEchos = fetchEchos;