// const request = require('supertest');
var request = require('request');

let server;

describe('/api/echos' , () => {
    let echoRequest;
    let fnCallback;
    server = require('../../index');

    const exec = () => {
        return request
          .post('http://localhost:8080/api/echos',echoRequest,fnCallback);
    }

    beforeAll( async () => { 
        });
        afterAll(() => { 
        });

    describe('POST /',  () => {
        it('should return a new echo request if request is valid' , async () => {
            echoRequest = { timestamp: Date.now() + 20*1000 , message: "hi" };
            fnCallback = function(error,response,body) {
                expect(response.statusCode).toBe(200);
                expect(body).toMatchObject({ timestamp:echoRequest.timestamp , message: echoRequest.message });    
            }
            exec();
        });

        it('should return 400 if message is empty' , async () => {
            echoRequest = { timestamp: Date.now() + 20*1000 , message: "" };
            fnCallback = function(error,response,body) {
                expect(response.statusCode).toBe(400);
            }
            exec();
        });

        it('should return 400 if message length is bigger than 255 ' , async () => {
            var str = new Array(256 + 1).join( 'x' );
            echoRequest = { timestamp: Date.now() + 20*1000 , message: str };
            fnCallback = function(error,response,body) {
                expect(response.statusCode).toBe(400);
            }
            exec();

        });


        it('should return 400 if timestamp is a past timestamp' , async () => {
            echoRequest = { timestamp: Date.now() - 20*1000 , message: "hello" };
            fnCallback = function(error,response,body) {
                expect(response.statusCode).toBe(400);
            }
            exec();
        });

        it('should return 400 if timestamp field does not exists' , async () => {
            echoRequest = { notTimestamp: Date.now() - 20*1000 , message: "hello" };
            fnCallback = function(error,response,body) {
                expect(response.statusCode).toBe(400);
            }
            exec();
        });

        it('should return 400 if message field does not exists' , async () => {
            echoRequest = { notTimestamp: Date.now() - 20*1000 , NOTmessage: "hello" };
            fnCallback = function(error,response,body) {
                expect(response.statusCode).toBe(400);
            }
            exec();
        });
    });
});