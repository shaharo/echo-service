const echo = require('../../models/echo');
const db = require('../../startup/db');
const uuidv4 = require('uuid/v4');

describe('echo model' , () => {
    beforeAll(() => {
        db.redis.flushall();
       
      });
      
      afterAll(() => {
        db.redis.flushall();
      });

    it('should return all echos present in db' , async () => {
        let echoRequest = { id:uuidv4(), message:  "hi", timestamp: Date.now()}

        await echo.scheduleEcho(echoRequest.timestamp,echoRequest);

        const res = await echo.fetchEchos(Date.now() + 20*1000);
        expect(JSON.parse(res[0])).toMatchObject({ timestamp:echoRequest.timestamp , message: echoRequest.message }); 
    });
});