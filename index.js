const winston = require('winston');
const express = require('express');
const app = express();
const db = require('./startup/db');
require('./startup/logging')();
scheduler = require('./startup/scheduler');
require('./startup/routes')(app);

let server;
const port = process.env.PORT || 8080

db.redis.connect().then(() => {
    winston.info("Connected to redis");
    server = app.listen(port,() => { winston.info("Server is running , port " + port);});
    scheduler();
});

module.exports = server;